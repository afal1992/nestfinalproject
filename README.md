## Description

Project backend that expose web services for client mobile. This project was build with 
[Nest](https://github.com/nestjs/nest), and this was deployed at cloud server.
 ## Frameworks
1. [Nest](https://github.com/nestjs/nest)
2. [Nodejs](https://nodejs.org/es/)
3. [Mysql](https://hub.docker.com/_/mysql)
4. [Typeorm](https://docs.nestjs.com/techniques/database)
5. [npm](https://www.npmjs.com/)

## Manual Installation

```bash
$ cd root working project
$ npm install -f
$ npm run start:debug
```

## Docker Installation

```bash
$ cd root working project
$ docker build -t backendtripli:1.0.0 .
$ docker-compose -f triplibackend.yml up -d
```
## License

[MIT licensed](LICENSE).
