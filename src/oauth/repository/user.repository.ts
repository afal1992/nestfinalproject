import { UserEntity } from 'src/database/model/user.entity';
import { Connection } from 'typeorm';

export const userRepository = [
  {
    provide: 'USER_REPOSITORY',
    useFactory: (connection: Connection) =>
      connection.getRepository(UserEntity),
    inject: ['DATABASE_CONNECTION'],
  },
];
