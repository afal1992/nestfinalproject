import {
  Body,
  Controller,
  Get,
  Inject,
  Post,
  Query,
  Request,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { CustomerService } from 'src/customer/customer-service';
import { GenericResponse } from './model/generic-response';
import { SignInRequest } from './model/signin-request';
import { OauthService } from './oauth-service';

export interface ISignIn {
  email: string;
  password: string;
  oauthType: string;
  oauthAccessToken: string;
}
export interface ISignUp {
  email: string;
  password: string;
  name: string;
  type?: string;
}

export interface ISignInGoogle {
  email: string;
  password: string;
  oauthType: string;
  oauthAccessToken: string;
}

@Controller('oauth')
export class OauthController {
  constructor(
    private readonly oauthService: OauthService,
    private customerService: CustomerService,
  ) {
    console.log(this.customerService.getHealthy());
  }

  @Post('/signIn')
  async signIn(@Body() body: SignInRequest): Promise<GenericResponse | null> {
    return await this.oauthService.singInGoogle(body);
  }

  @Get('/getPublicationPage')
  @UseGuards(AuthGuard('jwt'))
  async getPublicationPage(
    @Request() req: any,
    @Query() page,
  ): Promise<GenericResponse | null> {
    const { user } = req;
    console.log('User2...', user);
    return await this.oauthService.getPublicationPage(page);
  }
}
