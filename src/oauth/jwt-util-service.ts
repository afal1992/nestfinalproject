import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { IUserSession } from './oauth-service';

@Injectable()
export class JwtUtilService {
  constructor(private jwtService: JwtService) {}

  async generateAccessToken(user: IUserSession) {
    return this.jwtService.sign(user);
  }
}
