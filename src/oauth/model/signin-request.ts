import { IsNotEmpty } from 'class-validator';

export class SignInRequest {
  email!: string;

  password!: string;

  @IsNotEmpty()
  accessTokenOauth!: string;

  @IsNotEmpty()
  environment!: string;
}
