/**
 * Generic response for end points

 * @class
 */
export class GenericResponse {
  /**
   * A flag indicating if proccess was sucesfull
   * @type {boolean}
   */
  error = false;

  /**
   * Data response
   * @type {any}
   */
  data!: any;

  /**
   * Message response
   * @type {string}
   */
  message!: string;

  constructor(error: boolean, data: any, message: string) {
    this.error = error;
    this.data = data;
    this.message = message;
  }
}
