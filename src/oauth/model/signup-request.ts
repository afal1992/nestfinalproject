import { IsNotEmpty } from 'class-validator';
import { SignInRequest } from './signin-request';

export class SignUpRequest extends SignInRequest {
  @IsNotEmpty()
  name!: string;
}
