import { IsNotEmpty } from 'class-validator';

export class ResponseCheckGoogle {
  @IsNotEmpty()
  email!: string;
  @IsNotEmpty()
  name!: string;
  @IsNotEmpty()
  picture!: string;
}
