import { Inject, Injectable, Logger } from '@nestjs/common';
import { UserEntity } from 'src/database/model/user.entity';
import { FindManyOptions, Repository } from 'typeorm';
import { GenericResponse } from './model/generic-response';
import { validate } from 'class-validator';
import { GENERIC_ERROR, GENERIC_SUCCESS } from 'src/messages';
import { JwtUtilService } from './jwt-util-service';
import { SignInRequest } from './model/signin-request';
import axios from 'axios';
import { ResponseCheckGoogle } from './model/response-check-google';
import { ConfigService } from '@nestjs/config';
import { InjectRepository } from '@nestjs/typeorm';
export interface IUserSession {
  id: string;
  name: string;
  email: string;
  environment: string;
  picture: string;
}

@Injectable()
export class OauthService {
  private readonly logger = new Logger(OauthService.name);

  constructor(
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,
    private readonly jwtUtilService: JwtUtilService,
    private configService: ConfigService,
  ) {}
  async singIn(request: SignInRequest): Promise<GenericResponse | null> {
    try {
      const { email } = request;
      const conditions: FindManyOptions<UserEntity> = {
        where: {
          email,
        },
      };
      const responseSigIn = await this.userRepository.find(conditions);
      if (!responseSigIn || !responseSigIn[0]) return GENERIC_ERROR;

      const user = responseSigIn[0];
      const payload: IUserSession = {
        id: user.id,
        name: user.name,
        email: user.email,
        environment: 'TEST',
        picture: '',
      };
      const accessToken = await this.jwtUtilService.generateAccessToken(
        payload,
      );
      const response = { user: payload, accessToken };
      return new GenericResponse(false, response, 'sucessfull_message');
    } catch (e) {
      return GENERIC_ERROR;
    }
  }

  private async saveUser(
    responseGoogleLogin: ResponseCheckGoogle,
    environment: string,
  ): Promise<UserEntity | null> {
    try {
      const errorArray = await validate(responseGoogleLogin);

      if (errorArray && errorArray.length > 0) {
        this.logger.error('Err save user', errorArray);
        return null;
      }

      const user = new UserEntity(
        responseGoogleLogin.name,
        responseGoogleLogin.email,
        environment,
        responseGoogleLogin.picture,
      );

      return await this.userRepository.save(user);
    } catch (e) {
      return null;
    }
  }
  async singInGoogle(request: SignInRequest): Promise<GenericResponse | null> {
    try {
      const { accessTokenOauth } = request;
      const responseValidateOauth = await this.validateOauthGoogle(
        accessTokenOauth,
      );
      if (!responseValidateOauth) return GENERIC_ERROR;
      const conditions: FindManyOptions<UserEntity> = {
        where: {
          email: responseValidateOauth.email,
        },
      };
      const responseSigIn = await this.userRepository.find(conditions);
      let user = responseSigIn && responseSigIn[0] ? responseSigIn[0] : null;
      if (!user) {
        user = await this.saveUser(responseValidateOauth, request.environment);
      } else {
        /**
         * validate enviroment
         */
        if (request.environment !== user.environment) return GENERIC_ERROR;
      }

      if (!user) return GENERIC_ERROR;
      const payload: IUserSession = {
        id: user.id,
        name: user.name,
        email: user.email,
        environment: user.environment,
        picture: user.picture,
      };
      const accessToken = await this.jwtUtilService.generateAccessToken(
        payload,
      );
      user.password = '';
      const response = { user: payload, accessToken };
      return new GenericResponse(false, response, 'sucessfull_message');
    } catch (e) {
      return GENERIC_ERROR;
    }
  }

  async getPublicationPage(page: number) {
    try {
      return new GenericResponse(false, [page], '');
    } catch (e) {
      return new GenericResponse(false, [], '');
    }
  }

  private validateOauthGoogle = async (
    accessToken: string,
  ): Promise<ResponseCheckGoogle | null> => {
    try {
      const urlCheckGoogle =
        this.configService.get('URL_CHECK_GOOGLE') ||
        'https://www.googleapis.com/oauth2/v3/userinfo?access_token=';

      if (!urlCheckGoogle) return null;

      const response = await axios.get<ResponseCheckGoogle | null>(
        `${urlCheckGoogle}${accessToken}`,
        {
          timeout: 5000000,
        },
      );
      if (!response || !response.data) return null;

      return response.data;
    } catch (e) {
      this.logger.error(e);
      return null;
    }
  };
}
