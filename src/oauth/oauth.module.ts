import { Module } from '@nestjs/common';
import { DatabaseModule } from 'src/database/database.module';
import { JwtUtilService } from './jwt-util-service';
import { OauthController } from './oauth-controller';
import { OauthService } from './oauth-service';
import { userRepository } from './repository/user.repository';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { JwtStrategy } from './jwt.strategy';
import { CustomerModule } from 'src/customer/customer.module';
import { CustomerService } from 'src/customer/customer-service';
import { ConfigModule } from '@nestjs/config';
import { UserEntity } from 'src/database/model/user.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  controllers: [OauthController],
  imports: [
    DatabaseModule,
    PassportModule,
    CustomerModule,
    TypeOrmModule.forFeature([UserEntity], 'default'),
    JwtModule.register({
      secret: process.env.JWT_SECRET || 'afal1992',
      signOptions: { expiresIn: '8h' },
    }),
  ],
  providers: [OauthService, JwtUtilService, JwtStrategy],
})
export class OauthModule {}
