import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { IUserSession } from './oauth-service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: process.env.JWT_SECRET || 'afal1992',
    });
  }

  async validate(payload: IUserSession): Promise<IUserSession> {
    if (!payload) {
      throw new UnauthorizedException();
    }
    return { ...payload };
  }
}
