import { GenericResponse } from './oauth/model/generic-response';

export const GENERIC_ERROR = new GenericResponse(true, null, 'process_error');
export const GENERIC_SUCCESS = new GenericResponse(
  false,
  null,
  'process_success',
);
