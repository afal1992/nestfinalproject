import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        type: 'mysql',
        host: configService.get('DATABASE_MYSQL_HOST') || 'localhost',
        port:
          parseInt(configService.get('DATABASE_PORT') ?? '3306', 10) || 3306,
        database: configService.get('DATABASE_MYSQL_NAME') || 'portfolio_db',
        username: configService.get('DATABASE_MYSQL_USER') || 'root',
        password: configService.get('DATABASE_MYSQL_PASSWORD') || 'afal1992',
        synchronize: true,
        logging: true,
        entities: [__dirname + '/../**/*.entity{.ts,.js}'],
      }),
    }),
  ],
})
export class DatabaseModule {}
