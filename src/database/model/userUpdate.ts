import { IsNotEmpty } from 'class-validator';

export class UserUpdate {
  @IsNotEmpty()
  readonly phone: string;
  @IsNotEmpty()
  readonly direction: string;

  readonly facebook: string;

  readonly linkedin: string;

  readonly instagram: string;
}
