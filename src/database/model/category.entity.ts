import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('category')
export class Category {
  @PrimaryGeneratedColumn('uuid')
  readonly id: string;

  @Column({ type: 'varchar', length: 500, nullable: false })
  readonly name: string;

  @Column({ type: 'int' })
  readonly active: number;
}
