import * as bcrypt from 'bcrypt';
import { IsNotEmpty } from 'class-validator';
import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Category } from './category.entity';
import { Publication } from './publication.entity';

@Entity('users')
export class UserEntity {
  @PrimaryGeneratedColumn('uuid')
  readonly id: string;

  @IsNotEmpty()
  @Column({ type: 'varchar', length: 255, nullable: false })
  readonly name: string;

  @Column({ type: 'varchar', length: 70, nullable: true })
  password: string;

  @IsNotEmpty()
  @Column({ type: 'varchar', length: 255, nullable: false })
  readonly email: string;

  @IsNotEmpty()
  @Column({ type: 'varchar', length: 255, nullable: false })
  readonly environment: string;

  @Column({ type: 'varchar', length: 1000, nullable: false })
  readonly picture: string;

  @OneToMany(
    () => Publication,
    (publication: Publication) => publication.customer,
  )
  public publications: Publication[];
  /*@BeforeInsert()
  async hashPassword() {
    const salt = await bcrypt.genSalt();
    this.password = await bcrypt.hash(this.password, salt);
  }*/

  @Column({ type: 'varchar', length: 255, nullable: true })
  readonly phone: string;

  @Column({ type: 'varchar', length: 500, nullable: true })
  readonly direction: string;

  @Column({ type: 'varchar', length: 1500, nullable: true })
  readonly facebook: string;

  @Column({ type: 'varchar', length: 1500, nullable: true })
  readonly linkedin: string;

  @Column({ type: 'varchar', length: 1500, nullable: true })
  readonly instagram: string;

  @ManyToMany(() => Category, { cascade: true, eager: true })
  @JoinTable({
    name: 'user_category',
    joinColumn: { name: 'user_id', referencedColumnName: 'id' },
    inverseJoinColumn: { name: 'category_id', referencedColumnName: 'id' },
  })
  categories: Category[];

  async validatePassword(password: string): Promise<boolean> {
    return await bcrypt.compareSync(password, this.password);
  }

  constructor(
    name: string,
    email: string,
    environment: string,
    picture: string,
  ) {
    this.name = name;
    this.email = email;
    this.environment = environment;
    this.picture = picture;
  }
}
