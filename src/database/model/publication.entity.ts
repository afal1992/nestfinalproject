import { IsNotEmpty, Max } from 'class-validator';
import { PRIORITY_NORMAL, STATE_ACTIVE } from 'src/constants';
import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Category } from './category.entity';
import { PostulationEntity } from './postulation.entity';
import { UserEntity } from './user.entity';

@Entity('publication')
export class Publication {
  // @IsNotEmpty()
  @PrimaryGeneratedColumn('uuid')
  readonly id: string;

  //@IsNotEmpty()
  //@Max(500)
  @Column({ type: 'varchar', length: 500, nullable: false })
  readonly title: string;

  //@IsNotEmpty()
  //@Max(1000)
  @Column({ type: 'varchar', length: 1000, nullable: false })
  readonly description: string;

  @Column({ type: 'timestamp' })
  createDate: Date;

  @Column({ type: 'timestamp' })
  endDate: Date;

  createDateSring: string;
  endDateSring: string;

  //@IsNotEmpty()
  //@Max(255)
  @Column({
    type: 'varchar',
    length: 255,
    nullable: false,
    default: PRIORITY_NORMAL.id,
  })
  readonly priority: string;

  //@IsNotEmpty()
  //@Max(255)
  @Column({
    type: 'varchar',
    length: 255,
    nullable: false,
    default: STATE_ACTIVE.id,
  })
  readonly state: string;

  @ManyToOne(() => UserEntity, (user: UserEntity) => user.publications)
  customer: UserEntity;

  // @IsNotEmpty()
  @Max(100)
  @Column({ type: 'varchar', length: 100, nullable: false })
  readonly latitude: string;

  // @IsNotEmpty()
  // @Max(100)
  @Column({ type: 'varchar', length: 100, nullable: false })
  readonly longitude: string;

  //@IsNotEmpty()
  @ManyToMany(() => Category, { cascade: true, eager: true })
  @JoinTable({
    name: 'publication_category',
    joinColumn: { name: 'publication_id', referencedColumnName: 'id' },
    inverseJoinColumn: { name: 'category_id', referencedColumnName: 'id' },
  })
  categories: Category[];

  @OneToMany(
    () => PostulationEntity,
    (postulation: PostulationEntity) => postulation.publication,
  )
  public postulations: PostulationEntity[];
}
