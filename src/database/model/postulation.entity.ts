import { IsNotEmpty, Max } from 'class-validator';
import { POSTULATION_STATE_ARRAY } from 'src/constants';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Publication } from './publication.entity';
import { UserEntity } from './user.entity';

@Entity('postulation')
export class PostulationEntity {
  @PrimaryGeneratedColumn('uuid')
  readonly id: string;

  @IsNotEmpty()
  @Max(500)
  @Column({ type: 'varchar', length: 500, nullable: false })
  readonly description: string;

  @IsNotEmpty()
  @ManyToOne(
    () => Publication,
    (publication: Publication) => publication.postulations,
  )
  publication: Publication;

  @Column({
    nullable: false,
    default: () => 'CURRENT_TIMESTAMP',
    type: 'timestamp',
  })
  createDate: string;

  @ManyToOne(() => UserEntity, { cascade: true, eager: true })
  customer: UserEntity;

  @Column({
    type: 'varchar',
    nullable: false,
    default: POSTULATION_STATE_ARRAY[1],
  })
  state: string;
}
