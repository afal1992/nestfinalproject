import { Injectable } from '@nestjs/common';

@Injectable()
export class CustomerService {
  constructor() {}

  getHealthy(): string {
    return 'Up!';
  }
}
