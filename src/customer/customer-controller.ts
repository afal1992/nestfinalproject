import { Controller, Get, Inject } from '@nestjs/common';
import { getConnection, getRepository } from 'typeorm';
import { CustomerService } from './customer-service';
@Controller('customer')
export class CustomerController {
  constructor(private readonly customerService: CustomerService) {}

  @Get('/getHealthy')
  getHealthy(): string {
    console.log("Connection", getConnection);
    return this.customerService.getHealthy();
  }

  @Get('/getHealthy2')
  getHealthy2(): string {
    console.log("Connection", getConnection);
    return this.customerService.getHealthy();
  }
}
