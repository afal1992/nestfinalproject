import { Injectable, Logger } from '@nestjs/common';
import { Publication } from 'src/database/model/publication.entity';
import {
  IPaginationOptions,
  paginate,
  Pagination,
} from 'nestjs-typeorm-paginate';
import { InjectConnection } from '@nestjs/typeorm';
import { Connection, FindManyOptions } from 'typeorm';
import { GenericResponse } from 'src/oauth/model/generic-response';
import { GENERIC_ERROR } from 'src/messages';
import { PostulationEntity } from 'src/database/model/postulation.entity';
import { UserEntity } from 'src/database/model/user.entity';
import { async } from 'rxjs';
import { UserUpdate } from 'src/database/model/userUpdate';
import { response } from 'express';
import { Category } from 'src/database/model/category.entity';

@Injectable()
export class WorkerService {
  private readonly logger = new Logger(WorkerService.name);

  constructor(@InjectConnection('default') private connection: Connection) {}
  getPublicationArray = async (
    options: IPaginationOptions,
  ): Promise<Pagination<Publication>> => {
    const queryBuilder = await this.connection
      .getRepository(Publication)
      .createQueryBuilder('pub');
    queryBuilder.innerJoinAndSelect('pub.customer', 'u');
    queryBuilder.orderBy('pub.createDate', 'DESC');
    return paginate<Publication>(queryBuilder, options);
  };

  getPublicationById = async (id: string): Promise<Publication | null> => {
    try {
      this.logger.log('customerrr');

      const conditions: FindManyOptions<Publication> = {
        where: {
          id,
        },
      };
      const publicationResponse = await this.connection
        .getRepository(Publication)
        .find(conditions);
      if (!publicationResponse || publicationResponse.length === 0) return null;

      return publicationResponse[0];
    } catch (e) {
      this.logger.error(e);
      return null;
    }
  };

  savePostulation = async (
    postulation: PostulationEntity,
  ): Promise<GenericResponse> => {
    try {
      const countPostulationExists = await this.connection
        .getRepository(PostulationEntity)
        .createQueryBuilder('postulation')
        .where('publicationId = :publicationId', {
          publicationId: postulation.publication.id,
        })
        .andWhere('customerId = :customerId', {
          customerId: postulation.customer.id,
        })
        .getCount();

      if (countPostulationExists > 0)
        return new GenericResponse(true, undefined, 'postulation_exists');

      const responseSavePostulation = await this.connection
        .getRepository(PostulationEntity)
        .save(postulation);
      if (!responseSavePostulation) return GENERIC_ERROR;

      return new GenericResponse(
        false,
        responseSavePostulation,
        'publication_save_success',
      );
    } catch (e) {
      this.logger.error(e);
      return GENERIC_ERROR;
    }
  };

  getPostulationCustomerArray = async (
    userId: string,
  ): Promise<Array<PostulationEntity>> => {
    try {
      return (await this.connection
        .createQueryBuilder('publication', 'pub')
        .innerJoinAndSelect('pub.customer', 'customer')
        .innerJoinAndSelect('pub.postulations', 'post')
        .where('post.customerId = :customerId', {
          customerId: userId,
        })
        .execute()) as Array<PostulationEntity>;
    } catch (e) {
      this.logger.error(e);
      return [];
    }
  };

  getProfileUser = async (id): Promise<UserEntity | null> => {
    try {
      const conditions: FindManyOptions<UserEntity> = {
        where: {
          id,
        },
      };
      return await this.connection
        .getRepository(UserEntity)
        .findOne(conditions);
    } catch (e) {
      this.logger.error(e);
      return null;
    }
  };

  updateProfileUser = async (
    user: UserUpdate,
    userId: string,
  ): Promise<boolean> => {
    try {
      const responseUpdate = await this.connection
        .createQueryBuilder()
        .update(UserEntity)
        .set({
          phone: user.phone,
          direction: user.direction,
          facebook: user.facebook,
          linkedin: user.linkedin,
          instagram: user.instagram,
        })
        .where('id = :id', { id: userId })
        .execute();

      if (!responseUpdate) return false;

      return true;
    } catch (e) {
      this.logger.error(e);
      return false;
    }
  };

  addCategoryUser = async (
    categories: Array<Category>,
    userId: string,
  ): Promise<boolean> => {
    try {
      const conditions: FindManyOptions<UserEntity> = {
        where: {
          id: userId,
        },
      };
      const userEntity = await this.connection
        .getRepository(UserEntity)
        .findOne(conditions);

      if (!userEntity) return false;

      userEntity.categories.push(...categories);

      const responseUpdate = await this.connection
        .getRepository(UserEntity)
        .save(userEntity);

      if (!responseUpdate) return false;

      return true;
    } catch (e) {
      this.logger.error(e);
      return false;
    }
  };

  getCategoryArray = async (): Promise<Array<Category>> => {
    const conditions: FindManyOptions<Category> = {
      where: {
        active: 1,
      },
    };
    return await this.connection.getRepository(Category).find(conditions);
  };

  getPortfolioWorker = async (): Promise<Array<UserEntity>> => {
    try {
      const conditions: FindManyOptions<UserEntity> = {
        order: {
          name: 'ASC',
        },
        where: {
          environment: "WORKER"
        }
      };
      const dataCustomer = await this.connection
        .getRepository(UserEntity)
        .find(conditions);

      console.log('AA', dataCustomer);
      return dataCustomer;
    } catch (e) {
      this.logger.error(e);
      return [];
    }
  };
  getPublicationByUser = async (
    idUser: string,
  ): Promise<Array<Publication>> => {
    try {
      const conditions: FindManyOptions<Publication> = {
        order: {
          createDate: 'DESC',
        },
        where: {
          customer: { id: idUser },
        },
      };
      return await this.connection.getRepository(Publication).find(conditions);
    } catch (e) {
      this.logger.error(e);
      return [];
    }
  };

  savePublication = async (
    publication: Publication,
  ): Promise<boolean | false> => {
    try {
      publication.createDate = new Date(publication.createDateSring);
      publication.endDate = new Date(publication.endDateSring);

      const responseSavePub = await this.connection
        .getRepository(Publication)
        .save(publication);
      if (!responseSavePub) return false;
      return true;
    } catch (e) {
      this.logger.error(e);
      return false;
    }
  };

  getPostulationByPublication = async (
    idPublication: string,
  ): Promise<Array<PostulationEntity>> => {
    try {
      const conditions: FindManyOptions<PostulationEntity> = {
        order: {
          createDate: 'DESC',
        },
        where: {
          publication: { id: idPublication },
        },
      };
      const response = await this.connection
        .getRepository(PostulationEntity)
        .find(conditions);
      console.log('aca', response, idPublication);

      return response;
    } catch (e) {
      this.logger.error(e);
      return [];
    }
  };

  getPostulationWorker = async (
    userId: string,
  ): Promise<Array<PostulationEntity>> => {
    try {
      return (await this.connection
        .createQueryBuilder('publication', 'pub')
        .innerJoinAndSelect('pub.customer', 'customer')
        .innerJoinAndSelect('pub.postulations', 'post')
        .where('pub.customerId = :customerId', {
          customerId: userId,
        })
        .execute()) as Array<PostulationEntity>;
    } catch (e) {
      this.logger.error(e);
      return [];
    }
  };

  getPublicationArrayWorker = async (
    userId,
    options: IPaginationOptions,
  ): Promise<Pagination<Publication>> => {
    const queryBuilder = await this.connection
      .getRepository(Publication)
      .createQueryBuilder('pub');
    queryBuilder.innerJoinAndSelect('pub.customer', 'u');
    queryBuilder.where('pub.customer = :userId', {
      userId,
    });
    queryBuilder.orderBy('pub.createDate', 'DESC');
    return paginate<Publication>(queryBuilder, options);
  };

}
