import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DatabaseModule } from 'src/database/database.module';
import { Publication } from 'src/database/model/publication.entity';
import { WorkerController } from './worker.controller';
import { WorkerService } from './worker.service';

@Module({
  controllers: [WorkerController],
  providers: [WorkerService],
  imports: [TypeOrmModule.forFeature([Publication], 'default')],
})
export class WorkerModule {}
