import {
  Body,
  Controller,
  HttpStatus,
  Post,
  Req,
  Res,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { validate } from 'class-validator';
import { request, response } from 'express';
import { IPaginationOptions } from 'nestjs-typeorm-paginate';
import { async } from 'rxjs';
import { Category } from 'src/database/model/category.entity';
import { PostulationEntity } from 'src/database/model/postulation.entity';
import { Publication } from 'src/database/model/publication.entity';
import { IPublication } from 'src/database/model/publication.interface';
import { UserEntity } from 'src/database/model/user.entity';
import { UserUpdate } from 'src/database/model/userUpdate';
import { GENERIC_ERROR } from 'src/messages';
import { GenericResponse } from 'src/oauth/model/generic-response';
import { WorkerService } from './worker.service';

@Controller('worker')
export class WorkerController {
  constructor(private readonly workerService: WorkerService) {}
  @Post('/getPublicationArray')
  @UseGuards(AuthGuard('jwt'))
  async signIn(
    @Body() body: IPaginationOptions,
  ): Promise<GenericResponse | null> {
    const publicationArray = await this.workerService.getPublicationArray(body);
    return new GenericResponse(false, publicationArray, 'sucessfull_message');
  }

  @Post('/getPublicationById')
  @UseGuards(AuthGuard('jwt'))
  async getPublicationById(
    @Body() body: Publication,
  ): Promise<GenericResponse> {
    const publication = await this.workerService.getPublicationById(body.id);
    if (!publication) return GENERIC_ERROR;
    return new GenericResponse(false, publication, '');
  }

  @Post('/savePostulation')
  @UseGuards(AuthGuard('jwt'))
  async savePostulation(@Req() req, @Res() res): Promise<GenericResponse> {
    const customer = req.user as UserEntity;
    const postulation = req.body as PostulationEntity;
    postulation.customer = customer;
    const validatePostulation = await validate(postulation);
    if (validatePostulation && validatePostulation.length > 0) {
      return res
        .status(HttpStatus.BAD_REQUEST)
        .send('Field Required' + JSON.stringify(validatePostulation));
    }
    const response = await this.workerService.savePostulation(postulation);
    return res.status(HttpStatus.OK).send(response);
  }

  @Post('/getPostulationCustomerArray')
  @UseGuards(AuthGuard('jwt'))
  async getPostulationCustomerArray(@Req() req): Promise<GenericResponse> {
    const customer = req.user as UserEntity;
    const postulationArray =
      await this.workerService.getPostulationCustomerArray(customer.id);
    return new GenericResponse(false, postulationArray, '');
  }

  @Post('/getProfile')
  @UseGuards(AuthGuard('jwt'))
  async getProfile(@Req() req): Promise<GenericResponse> {
    const userEntity = req.user as UserEntity;
    const profile = await this.workerService.getProfileUser(userEntity.id);
    if (!profile) return GENERIC_ERROR;
    return new GenericResponse(false, profile, '');
  }

  @Post('/updateProfile')
  @UseGuards(AuthGuard('jwt'))
  async updateProfile(
    @Req() req,
    @Body() updateUser: UserUpdate,
  ): Promise<GenericResponse> {
    const userEntity = req.user as UserEntity;
    const resultUpdate = await this.workerService.updateProfileUser(
      updateUser,
      userEntity.id,
    );

    if (!resultUpdate) return GENERIC_ERROR;
    return new GenericResponse(false, {}, '');
  }

  @Post('/addCategoryUser')
  @UseGuards(AuthGuard('jwt'))
  async addCategoryUser(
    @Req() req,
    @Body() categories: Array<Category>,
  ): Promise<GenericResponse> {
    const userEntity = req.user as UserEntity;
    const resultUpdate = await this.workerService.addCategoryUser(
      categories,
      userEntity.id,
    );
    if (!resultUpdate) return GENERIC_ERROR;
    return new GenericResponse(false, {}, '');
  }

  @Post('/getCategoryArray')
  @UseGuards(AuthGuard('jwt'))
  async getCategoryArray(): Promise<GenericResponse> {
    const categories = await this.workerService.getCategoryArray();
    if (!categories) return GENERIC_ERROR;
    return new GenericResponse(false, categories, '');
  }

  @Post('/getPortfolioWorker')
  @UseGuards(AuthGuard('jwt'))
  async getPortfolioWorker(): Promise<GenericResponse> {
    const workerArray = await this.workerService.getPortfolioWorker();
    if (!workerArray) return GENERIC_ERROR;
    return new GenericResponse(false, workerArray, '');
  }
  @Post('/getPublicationByUser')
  @UseGuards(AuthGuard('jwt'))
  async getPublicationByUser(@Req() req): Promise<GenericResponse> {
    const user = req.user as UserEntity;
    const workerArray = await this.workerService.getPublicationByUser(user.id);
    if (!workerArray) return GENERIC_ERROR;
    return new GenericResponse(false, workerArray, '');
  }
  @Post('/savePublication')
  @UseGuards(AuthGuard('jwt'))
  async savePublication(
    @Req() request,
    @Body() publication: Publication,
  ): Promise<GenericResponse> {
    const user = request.user as UserEntity;
    publication.customer = user;
    console.log('data', publication);
    const responseSave = await this.workerService.savePublication(publication);
    if (!responseSave) return GENERIC_ERROR;
    return new GenericResponse(false, {}, '');
  }

  @Post('/getPostulationByPublication')
  @UseGuards(AuthGuard('jwt'))
  async getPostulationByPublication(
    @Body() data: IPublication,
  ): Promise<GenericResponse> {
    const response = await this.workerService.getPostulationByPublication(
      data.idPublication,
    );
    if (!response) return GENERIC_ERROR;

    return new GenericResponse(false, response, '');
  }

  @Post('/getPostulationWorker')
  @UseGuards(AuthGuard('jwt'))
  async getPostulationWorker(@Req() req): Promise<GenericResponse> {
    const user = req.user as UserEntity;
    const response = await this.workerService.getPostulationWorker(user.id);
    if (!response) return GENERIC_ERROR;
    return new GenericResponse(false, response, '');
  }

  @Post('/getPublicationArrayWorker')
  @UseGuards(AuthGuard('jwt'))
  async getPublicationArrayWorker(
    @Req() req,
    @Body() body: IPaginationOptions,
  ): Promise<GenericResponse> {
    const user = req.user as UserEntity;
    const response = await this.workerService.getPublicationArrayWorker(
      user.id,
      body,
    );
    if (!response) return GENERIC_ERROR;
    return new GenericResponse(false, response, '');
  }
}
