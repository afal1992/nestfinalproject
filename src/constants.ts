export const OAUTH_GOOGLE = 'OAUTH_GOOGLE';
export const OAUTH_EMAIL = 'OAUTH_EMAIL';
export const PRIORITY_NORMAL = { id: 'normal', label: 'priority_normal' };
export const PRIORITY_LOW = { id: 'low', label: 'priority_low' };
export const PRIORITY_HIGH = { id: 'high', label: 'priority_high' };
export const PRIORITY_ARRAY = [PRIORITY_HIGH, PRIORITY_LOW, PRIORITY_NORMAL];
export const STATE_ACTIVE = { id: 'active', label: 'state_active' };
export const STATE_EXPIRED = { id: 'expired', label: 'state_expired' };
export const STATE_ARRAY = [STATE_ACTIVE, STATE_EXPIRED];
export const STATE_ACCEPT_POSTULATION = 'accept';
export const STATE_PENDING_POSTULATION = 'pending';
export const STATE_REFUSED_POSTULATION = 'refused';
export const POSTULATION_STATE_ARRAY = [
  STATE_ACCEPT_POSTULATION,
  STATE_PENDING_POSTULATION,
  STATE_REFUSED_POSTULATION,
];
