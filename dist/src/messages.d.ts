import { GenericResponse } from './oauth/model/generic-response';
export declare const GENERIC_ERROR: GenericResponse;
export declare const GENERIC_SUCCESS: GenericResponse;
