"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var WorkerService_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.WorkerService = void 0;
const common_1 = require("@nestjs/common");
const publication_entity_1 = require("../database/model/publication.entity");
const nestjs_typeorm_paginate_1 = require("nestjs-typeorm-paginate");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const generic_response_1 = require("../oauth/model/generic-response");
const messages_1 = require("../messages");
const postulation_entity_1 = require("../database/model/postulation.entity");
const user_entity_1 = require("../database/model/user.entity");
const category_entity_1 = require("../database/model/category.entity");
let WorkerService = WorkerService_1 = class WorkerService {
    constructor(connection) {
        this.connection = connection;
        this.logger = new common_1.Logger(WorkerService_1.name);
        this.getPublicationArray = async (options) => {
            const queryBuilder = await this.connection
                .getRepository(publication_entity_1.Publication)
                .createQueryBuilder('pub');
            queryBuilder.innerJoinAndSelect('pub.customer', 'u');
            queryBuilder.orderBy('pub.createDate', 'DESC');
            return (0, nestjs_typeorm_paginate_1.paginate)(queryBuilder, options);
        };
        this.getPublicationById = async (id) => {
            try {
                this.logger.log('customerrr');
                const conditions = {
                    where: {
                        id,
                    },
                };
                const publicationResponse = await this.connection
                    .getRepository(publication_entity_1.Publication)
                    .find(conditions);
                if (!publicationResponse || publicationResponse.length === 0)
                    return null;
                return publicationResponse[0];
            }
            catch (e) {
                this.logger.error(e);
                return null;
            }
        };
        this.savePostulation = async (postulation) => {
            try {
                const countPostulationExists = await this.connection
                    .getRepository(postulation_entity_1.PostulationEntity)
                    .createQueryBuilder('postulation')
                    .where('publicationId = :publicationId', {
                    publicationId: postulation.publication.id,
                })
                    .andWhere('customerId = :customerId', {
                    customerId: postulation.customer.id,
                })
                    .getCount();
                if (countPostulationExists > 0)
                    return new generic_response_1.GenericResponse(true, undefined, 'postulation_exists');
                const responseSavePostulation = await this.connection
                    .getRepository(postulation_entity_1.PostulationEntity)
                    .save(postulation);
                if (!responseSavePostulation)
                    return messages_1.GENERIC_ERROR;
                return new generic_response_1.GenericResponse(false, responseSavePostulation, 'publication_save_success');
            }
            catch (e) {
                this.logger.error(e);
                return messages_1.GENERIC_ERROR;
            }
        };
        this.getPostulationCustomerArray = async (userId) => {
            try {
                return (await this.connection
                    .createQueryBuilder('publication', 'pub')
                    .innerJoinAndSelect('pub.customer', 'customer')
                    .innerJoinAndSelect('pub.postulations', 'post')
                    .where('post.customerId = :customerId', {
                    customerId: userId,
                })
                    .execute());
            }
            catch (e) {
                this.logger.error(e);
                return [];
            }
        };
        this.getProfileUser = async (id) => {
            try {
                const conditions = {
                    where: {
                        id,
                    },
                };
                return await this.connection
                    .getRepository(user_entity_1.UserEntity)
                    .findOne(conditions);
            }
            catch (e) {
                this.logger.error(e);
                return null;
            }
        };
        this.updateProfileUser = async (user, userId) => {
            try {
                const responseUpdate = await this.connection
                    .createQueryBuilder()
                    .update(user_entity_1.UserEntity)
                    .set({
                    phone: user.phone,
                    direction: user.direction,
                    facebook: user.facebook,
                    linkedin: user.linkedin,
                    instagram: user.instagram,
                })
                    .where('id = :id', { id: userId })
                    .execute();
                if (!responseUpdate)
                    return false;
                return true;
            }
            catch (e) {
                this.logger.error(e);
                return false;
            }
        };
        this.addCategoryUser = async (categories, userId) => {
            try {
                const conditions = {
                    where: {
                        id: userId,
                    },
                };
                const userEntity = await this.connection
                    .getRepository(user_entity_1.UserEntity)
                    .findOne(conditions);
                if (!userEntity)
                    return false;
                userEntity.categories.push(...categories);
                const responseUpdate = await this.connection
                    .getRepository(user_entity_1.UserEntity)
                    .save(userEntity);
                if (!responseUpdate)
                    return false;
                return true;
            }
            catch (e) {
                this.logger.error(e);
                return false;
            }
        };
        this.getCategoryArray = async () => {
            const conditions = {
                where: {
                    active: 1,
                },
            };
            return await this.connection.getRepository(category_entity_1.Category).find(conditions);
        };
        this.getPortfolioWorker = async () => {
            try {
                const conditions = {
                    order: {
                        name: 'ASC',
                    },
                };
                const dataCustomer = await this.connection
                    .getRepository(user_entity_1.UserEntity)
                    .find(conditions);
                console.log('AA', dataCustomer);
                return dataCustomer;
            }
            catch (e) {
                this.logger.error(e);
                return [];
            }
        };
        this.getPublicationByUser = async (idUser) => {
            try {
                const conditions = {
                    order: {
                        createDate: 'DESC',
                    },
                    where: {
                        customer: { id: idUser },
                    },
                };
                return await this.connection.getRepository(publication_entity_1.Publication).find(conditions);
            }
            catch (e) {
                this.logger.error(e);
                return [];
            }
        };
        this.savePublication = async (publication) => {
            try {
                publication.createDate = new Date(publication.createDateSring);
                publication.endDate = new Date(publication.endDateSring);
                const responseSavePub = await this.connection
                    .getRepository(publication_entity_1.Publication)
                    .save(publication);
                if (!responseSavePub)
                    return false;
                return true;
            }
            catch (e) {
                this.logger.error(e);
                return false;
            }
        };
        this.getPostulationByPublication = async (idPublication) => {
            try {
                const conditions = {
                    order: {
                        createDate: 'DESC',
                    },
                    where: {
                        publication: { id: idPublication },
                    },
                };
                const response = await this.connection
                    .getRepository(postulation_entity_1.PostulationEntity)
                    .find(conditions);
                console.log('aca', response, idPublication);
                return response;
            }
            catch (e) {
                this.logger.error(e);
                return [];
            }
        };
        this.getPostulationWorker = async (userId) => {
            try {
                return (await this.connection
                    .createQueryBuilder('publication', 'pub')
                    .innerJoinAndSelect('pub.customer', 'customer')
                    .innerJoinAndSelect('pub.postulations', 'post')
                    .where('pub.customerId = :customerId', {
                    customerId: userId,
                })
                    .execute());
            }
            catch (e) {
                this.logger.error(e);
                return [];
            }
        };
        this.getPublicationArrayWorker = async (userId, options) => {
            const queryBuilder = await this.connection
                .getRepository(publication_entity_1.Publication)
                .createQueryBuilder('pub');
            queryBuilder.innerJoinAndSelect('pub.customer', 'u');
            queryBuilder.where('pub.customer = :userId', {
                userId,
            });
            queryBuilder.orderBy('pub.createDate', 'DESC');
            return (0, nestjs_typeorm_paginate_1.paginate)(queryBuilder, options);
        };
    }
};
WorkerService = WorkerService_1 = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectConnection)('default')),
    __metadata("design:paramtypes", [typeorm_2.Connection])
], WorkerService);
exports.WorkerService = WorkerService;
//# sourceMappingURL=worker.service.js.map