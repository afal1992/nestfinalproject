import { Publication } from 'src/database/model/publication.entity';
import { IPaginationOptions, Pagination } from 'nestjs-typeorm-paginate';
import { Connection } from 'typeorm';
import { GenericResponse } from 'src/oauth/model/generic-response';
import { PostulationEntity } from 'src/database/model/postulation.entity';
import { UserEntity } from 'src/database/model/user.entity';
import { UserUpdate } from 'src/database/model/userUpdate';
import { Category } from 'src/database/model/category.entity';
export declare class WorkerService {
    private connection;
    private readonly logger;
    constructor(connection: Connection);
    getPublicationArray: (options: IPaginationOptions) => Promise<Pagination<Publication>>;
    getPublicationById: (id: string) => Promise<Publication | null>;
    savePostulation: (postulation: PostulationEntity) => Promise<GenericResponse>;
    getPostulationCustomerArray: (userId: string) => Promise<Array<PostulationEntity>>;
    getProfileUser: (id: any) => Promise<UserEntity | null>;
    updateProfileUser: (user: UserUpdate, userId: string) => Promise<boolean>;
    addCategoryUser: (categories: Array<Category>, userId: string) => Promise<boolean>;
    getCategoryArray: () => Promise<Array<Category>>;
    getPortfolioWorker: () => Promise<Array<UserEntity>>;
    getPublicationByUser: (idUser: string) => Promise<Array<Publication>>;
    savePublication: (publication: Publication) => Promise<boolean | false>;
    getPostulationByPublication: (idPublication: string) => Promise<Array<PostulationEntity>>;
    getPostulationWorker: (userId: string) => Promise<Array<PostulationEntity>>;
    getPublicationArrayWorker: (userId: any, options: IPaginationOptions) => Promise<Pagination<Publication>>;
}
