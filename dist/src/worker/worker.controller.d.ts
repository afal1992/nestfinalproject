import { IPaginationOptions } from 'nestjs-typeorm-paginate';
import { Category } from 'src/database/model/category.entity';
import { Publication } from 'src/database/model/publication.entity';
import { IPublication } from 'src/database/model/publication.interface';
import { UserUpdate } from 'src/database/model/userUpdate';
import { GenericResponse } from 'src/oauth/model/generic-response';
import { WorkerService } from './worker.service';
export declare class WorkerController {
    private readonly workerService;
    constructor(workerService: WorkerService);
    signIn(body: IPaginationOptions): Promise<GenericResponse | null>;
    getPublicationById(body: Publication): Promise<GenericResponse>;
    savePostulation(req: any, res: any): Promise<GenericResponse>;
    getPostulationCustomerArray(req: any): Promise<GenericResponse>;
    getProfile(req: any): Promise<GenericResponse>;
    updateProfile(req: any, updateUser: UserUpdate): Promise<GenericResponse>;
    addCategoryUser(req: any, categories: Array<Category>): Promise<GenericResponse>;
    getCategoryArray(): Promise<GenericResponse>;
    getPortfolioWorker(): Promise<GenericResponse>;
    getPublicationByUser(req: any): Promise<GenericResponse>;
    savePublication(request: any, publication: Publication): Promise<GenericResponse>;
    getPostulationByPublication(data: IPublication): Promise<GenericResponse>;
    getPostulationWorker(req: any): Promise<GenericResponse>;
    getPublicationArrayWorker(req: any, body: IPaginationOptions): Promise<GenericResponse>;
}
