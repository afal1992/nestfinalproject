"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WorkerController = void 0;
const common_1 = require("@nestjs/common");
const passport_1 = require("@nestjs/passport");
const class_validator_1 = require("class-validator");
const publication_entity_1 = require("../database/model/publication.entity");
const userUpdate_1 = require("../database/model/userUpdate");
const messages_1 = require("../messages");
const generic_response_1 = require("../oauth/model/generic-response");
const worker_service_1 = require("./worker.service");
let WorkerController = class WorkerController {
    constructor(workerService) {
        this.workerService = workerService;
    }
    async signIn(body) {
        const publicationArray = await this.workerService.getPublicationArray(body);
        return new generic_response_1.GenericResponse(false, publicationArray, 'sucessfull_message');
    }
    async getPublicationById(body) {
        const publication = await this.workerService.getPublicationById(body.id);
        if (!publication)
            return messages_1.GENERIC_ERROR;
        return new generic_response_1.GenericResponse(false, publication, '');
    }
    async savePostulation(req, res) {
        const customer = req.user;
        const postulation = req.body;
        postulation.customer = customer;
        const validatePostulation = await (0, class_validator_1.validate)(postulation);
        if (validatePostulation && validatePostulation.length > 0) {
            return res
                .status(common_1.HttpStatus.BAD_REQUEST)
                .send('Field Required' + JSON.stringify(validatePostulation));
        }
        const response = await this.workerService.savePostulation(postulation);
        return res.status(common_1.HttpStatus.OK).send(response);
    }
    async getPostulationCustomerArray(req) {
        const customer = req.user;
        const postulationArray = await this.workerService.getPostulationCustomerArray(customer.id);
        return new generic_response_1.GenericResponse(false, postulationArray, '');
    }
    async getProfile(req) {
        const userEntity = req.user;
        const profile = await this.workerService.getProfileUser(userEntity.id);
        if (!profile)
            return messages_1.GENERIC_ERROR;
        return new generic_response_1.GenericResponse(false, profile, '');
    }
    async updateProfile(req, updateUser) {
        const userEntity = req.user;
        const resultUpdate = await this.workerService.updateProfileUser(updateUser, userEntity.id);
        if (!resultUpdate)
            return messages_1.GENERIC_ERROR;
        return new generic_response_1.GenericResponse(false, {}, '');
    }
    async addCategoryUser(req, categories) {
        const userEntity = req.user;
        const resultUpdate = await this.workerService.addCategoryUser(categories, userEntity.id);
        if (!resultUpdate)
            return messages_1.GENERIC_ERROR;
        return new generic_response_1.GenericResponse(false, {}, '');
    }
    async getCategoryArray() {
        const categories = await this.workerService.getCategoryArray();
        if (!categories)
            return messages_1.GENERIC_ERROR;
        return new generic_response_1.GenericResponse(false, categories, '');
    }
    async getPortfolioWorker() {
        const workerArray = await this.workerService.getPortfolioWorker();
        if (!workerArray)
            return messages_1.GENERIC_ERROR;
        return new generic_response_1.GenericResponse(false, workerArray, '');
    }
    async getPublicationByUser(req) {
        const user = req.user;
        const workerArray = await this.workerService.getPublicationByUser(user.id);
        if (!workerArray)
            return messages_1.GENERIC_ERROR;
        return new generic_response_1.GenericResponse(false, workerArray, '');
    }
    async savePublication(request, publication) {
        const user = request.user;
        publication.customer = user;
        console.log('data', publication);
        const responseSave = await this.workerService.savePublication(publication);
        if (!responseSave)
            return messages_1.GENERIC_ERROR;
        return new generic_response_1.GenericResponse(false, {}, '');
    }
    async getPostulationByPublication(data) {
        const response = await this.workerService.getPostulationByPublication(data.idPublication);
        if (!response)
            return messages_1.GENERIC_ERROR;
        return new generic_response_1.GenericResponse(false, response, '');
    }
    async getPostulationWorker(req) {
        const user = req.user;
        const response = await this.workerService.getPostulationWorker(user.id);
        if (!response)
            return messages_1.GENERIC_ERROR;
        return new generic_response_1.GenericResponse(false, response, '');
    }
    async getPublicationArrayWorker(req, body) {
        const user = req.user;
        const response = await this.workerService.getPublicationArrayWorker(user.id, body);
        if (!response)
            return messages_1.GENERIC_ERROR;
        return new generic_response_1.GenericResponse(false, response, '');
    }
};
__decorate([
    (0, common_1.Post)('/getPublicationArray'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('jwt')),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], WorkerController.prototype, "signIn", null);
__decorate([
    (0, common_1.Post)('/getPublicationById'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('jwt')),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [publication_entity_1.Publication]),
    __metadata("design:returntype", Promise)
], WorkerController.prototype, "getPublicationById", null);
__decorate([
    (0, common_1.Post)('/savePostulation'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('jwt')),
    __param(0, (0, common_1.Req)()),
    __param(1, (0, common_1.Res)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], WorkerController.prototype, "savePostulation", null);
__decorate([
    (0, common_1.Post)('/getPostulationCustomerArray'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('jwt')),
    __param(0, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], WorkerController.prototype, "getPostulationCustomerArray", null);
__decorate([
    (0, common_1.Post)('/getProfile'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('jwt')),
    __param(0, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], WorkerController.prototype, "getProfile", null);
__decorate([
    (0, common_1.Post)('/updateProfile'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('jwt')),
    __param(0, (0, common_1.Req)()),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, userUpdate_1.UserUpdate]),
    __metadata("design:returntype", Promise)
], WorkerController.prototype, "updateProfile", null);
__decorate([
    (0, common_1.Post)('/addCategoryUser'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('jwt')),
    __param(0, (0, common_1.Req)()),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Array]),
    __metadata("design:returntype", Promise)
], WorkerController.prototype, "addCategoryUser", null);
__decorate([
    (0, common_1.Post)('/getCategoryArray'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('jwt')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], WorkerController.prototype, "getCategoryArray", null);
__decorate([
    (0, common_1.Post)('/getPortfolioWorker'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('jwt')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], WorkerController.prototype, "getPortfolioWorker", null);
__decorate([
    (0, common_1.Post)('/getPublicationByUser'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('jwt')),
    __param(0, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], WorkerController.prototype, "getPublicationByUser", null);
__decorate([
    (0, common_1.Post)('/savePublication'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('jwt')),
    __param(0, (0, common_1.Req)()),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, publication_entity_1.Publication]),
    __metadata("design:returntype", Promise)
], WorkerController.prototype, "savePublication", null);
__decorate([
    (0, common_1.Post)('/getPostulationByPublication'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('jwt')),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], WorkerController.prototype, "getPostulationByPublication", null);
__decorate([
    (0, common_1.Post)('/getPostulationWorker'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('jwt')),
    __param(0, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], WorkerController.prototype, "getPostulationWorker", null);
__decorate([
    (0, common_1.Post)('/getPublicationArrayWorker'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('jwt')),
    __param(0, (0, common_1.Req)()),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], WorkerController.prototype, "getPublicationArrayWorker", null);
WorkerController = __decorate([
    (0, common_1.Controller)('worker'),
    __metadata("design:paramtypes", [worker_service_1.WorkerService])
], WorkerController);
exports.WorkerController = WorkerController;
//# sourceMappingURL=worker.controller.js.map