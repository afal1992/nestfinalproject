"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Publication = void 0;
const class_validator_1 = require("class-validator");
const constants_1 = require("../../constants");
const typeorm_1 = require("typeorm");
const category_entity_1 = require("./category.entity");
const postulation_entity_1 = require("./postulation.entity");
const user_entity_1 = require("./user.entity");
let Publication = class Publication {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)('uuid'),
    __metadata("design:type", String)
], Publication.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', length: 500, nullable: false }),
    __metadata("design:type", String)
], Publication.prototype, "title", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', length: 1000, nullable: false }),
    __metadata("design:type", String)
], Publication.prototype, "description", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], Publication.prototype, "createDate", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'timestamp' }),
    __metadata("design:type", Date)
], Publication.prototype, "endDate", void 0);
__decorate([
    (0, typeorm_1.Column)({
        type: 'varchar',
        length: 255,
        nullable: false,
        default: constants_1.PRIORITY_NORMAL.id,
    }),
    __metadata("design:type", String)
], Publication.prototype, "priority", void 0);
__decorate([
    (0, typeorm_1.Column)({
        type: 'varchar',
        length: 255,
        nullable: false,
        default: constants_1.STATE_ACTIVE.id,
    }),
    __metadata("design:type", String)
], Publication.prototype, "state", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => user_entity_1.UserEntity, (user) => user.publications),
    __metadata("design:type", user_entity_1.UserEntity)
], Publication.prototype, "customer", void 0);
__decorate([
    (0, class_validator_1.Max)(100),
    (0, typeorm_1.Column)({ type: 'varchar', length: 100, nullable: false }),
    __metadata("design:type", String)
], Publication.prototype, "latitude", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', length: 100, nullable: false }),
    __metadata("design:type", String)
], Publication.prototype, "longitude", void 0);
__decorate([
    (0, typeorm_1.ManyToMany)(() => category_entity_1.Category, { cascade: true, eager: true }),
    (0, typeorm_1.JoinTable)({
        name: 'publication_category',
        joinColumn: { name: 'publication_id', referencedColumnName: 'id' },
        inverseJoinColumn: { name: 'category_id', referencedColumnName: 'id' },
    }),
    __metadata("design:type", Array)
], Publication.prototype, "categories", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => postulation_entity_1.PostulationEntity, (postulation) => postulation.publication),
    __metadata("design:type", Array)
], Publication.prototype, "postulations", void 0);
Publication = __decorate([
    (0, typeorm_1.Entity)('publication')
], Publication);
exports.Publication = Publication;
//# sourceMappingURL=publication.entity.js.map