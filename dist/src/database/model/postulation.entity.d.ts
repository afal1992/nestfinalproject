import { Publication } from './publication.entity';
import { UserEntity } from './user.entity';
export declare class PostulationEntity {
    readonly id: string;
    readonly description: string;
    publication: Publication;
    createDate: string;
    customer: UserEntity;
    state: string;
}
