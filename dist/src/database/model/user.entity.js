"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserEntity = void 0;
const bcrypt = require("bcrypt");
const class_validator_1 = require("class-validator");
const typeorm_1 = require("typeorm");
const category_entity_1 = require("./category.entity");
const publication_entity_1 = require("./publication.entity");
let UserEntity = class UserEntity {
    constructor(name, email, environment, picture) {
        this.name = name;
        this.email = email;
        this.environment = environment;
        this.picture = picture;
    }
    async validatePassword(password) {
        return await bcrypt.compareSync(password, this.password);
    }
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)('uuid'),
    __metadata("design:type", String)
], UserEntity.prototype, "id", void 0);
__decorate([
    (0, class_validator_1.IsNotEmpty)(),
    (0, typeorm_1.Column)({ type: 'varchar', length: 255, nullable: false }),
    __metadata("design:type", String)
], UserEntity.prototype, "name", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', length: 70, nullable: true }),
    __metadata("design:type", String)
], UserEntity.prototype, "password", void 0);
__decorate([
    (0, class_validator_1.IsNotEmpty)(),
    (0, typeorm_1.Column)({ type: 'varchar', length: 255, nullable: false }),
    __metadata("design:type", String)
], UserEntity.prototype, "email", void 0);
__decorate([
    (0, class_validator_1.IsNotEmpty)(),
    (0, typeorm_1.Column)({ type: 'varchar', length: 255, nullable: false }),
    __metadata("design:type", String)
], UserEntity.prototype, "environment", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', length: 1000, nullable: false }),
    __metadata("design:type", String)
], UserEntity.prototype, "picture", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => publication_entity_1.Publication, (publication) => publication.customer),
    __metadata("design:type", Array)
], UserEntity.prototype, "publications", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', length: 255, nullable: true }),
    __metadata("design:type", String)
], UserEntity.prototype, "phone", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', length: 500, nullable: true }),
    __metadata("design:type", String)
], UserEntity.prototype, "direction", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', length: 1500, nullable: true }),
    __metadata("design:type", String)
], UserEntity.prototype, "facebook", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', length: 1500, nullable: true }),
    __metadata("design:type", String)
], UserEntity.prototype, "linkedin", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', length: 1500, nullable: true }),
    __metadata("design:type", String)
], UserEntity.prototype, "instagram", void 0);
__decorate([
    (0, typeorm_1.ManyToMany)(() => category_entity_1.Category, { cascade: true, eager: true }),
    (0, typeorm_1.JoinTable)({
        name: 'user_category',
        joinColumn: { name: 'user_id', referencedColumnName: 'id' },
        inverseJoinColumn: { name: 'category_id', referencedColumnName: 'id' },
    }),
    __metadata("design:type", Array)
], UserEntity.prototype, "categories", void 0);
UserEntity = __decorate([
    (0, typeorm_1.Entity)('users'),
    __metadata("design:paramtypes", [String, String, String, String])
], UserEntity);
exports.UserEntity = UserEntity;
//# sourceMappingURL=user.entity.js.map