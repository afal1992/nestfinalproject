export declare class UserUpdate {
    readonly phone: string;
    readonly direction: string;
    readonly facebook: string;
    readonly linkedin: string;
    readonly instagram: string;
}
