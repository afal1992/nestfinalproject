import { Category } from './category.entity';
import { Publication } from './publication.entity';
export declare class UserEntity {
    readonly id: string;
    readonly name: string;
    password: string;
    readonly email: string;
    readonly environment: string;
    readonly picture: string;
    publications: Publication[];
    readonly phone: string;
    readonly direction: string;
    readonly facebook: string;
    readonly linkedin: string;
    readonly instagram: string;
    categories: Category[];
    validatePassword(password: string): Promise<boolean>;
    constructor(name: string, email: string, environment: string, picture: string);
}
