"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PostulationEntity = void 0;
const class_validator_1 = require("class-validator");
const constants_1 = require("../../constants");
const typeorm_1 = require("typeorm");
const publication_entity_1 = require("./publication.entity");
const user_entity_1 = require("./user.entity");
let PostulationEntity = class PostulationEntity {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)('uuid'),
    __metadata("design:type", String)
], PostulationEntity.prototype, "id", void 0);
__decorate([
    (0, class_validator_1.IsNotEmpty)(),
    (0, class_validator_1.Max)(500),
    (0, typeorm_1.Column)({ type: 'varchar', length: 500, nullable: false }),
    __metadata("design:type", String)
], PostulationEntity.prototype, "description", void 0);
__decorate([
    (0, class_validator_1.IsNotEmpty)(),
    (0, typeorm_1.ManyToOne)(() => publication_entity_1.Publication, (publication) => publication.postulations),
    __metadata("design:type", publication_entity_1.Publication)
], PostulationEntity.prototype, "publication", void 0);
__decorate([
    (0, typeorm_1.Column)({
        nullable: false,
        default: () => 'CURRENT_TIMESTAMP',
        type: 'timestamp',
    }),
    __metadata("design:type", String)
], PostulationEntity.prototype, "createDate", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => user_entity_1.UserEntity, { cascade: true, eager: true }),
    __metadata("design:type", user_entity_1.UserEntity)
], PostulationEntity.prototype, "customer", void 0);
__decorate([
    (0, typeorm_1.Column)({
        type: 'varchar',
        nullable: false,
        default: constants_1.POSTULATION_STATE_ARRAY[1],
    }),
    __metadata("design:type", String)
], PostulationEntity.prototype, "state", void 0);
PostulationEntity = __decorate([
    (0, typeorm_1.Entity)('postulation')
], PostulationEntity);
exports.PostulationEntity = PostulationEntity;
//# sourceMappingURL=postulation.entity.js.map