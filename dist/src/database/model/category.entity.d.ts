export declare class Category {
    readonly id: string;
    readonly name: string;
    readonly active: number;
}
