import { Category } from './category.entity';
import { PostulationEntity } from './postulation.entity';
import { UserEntity } from './user.entity';
export declare class Publication {
    readonly id: string;
    readonly title: string;
    readonly description: string;
    createDate: Date;
    endDate: Date;
    createDateSring: string;
    endDateSring: string;
    readonly priority: string;
    readonly state: string;
    customer: UserEntity;
    readonly latitude: string;
    readonly longitude: string;
    categories: Category[];
    postulations: PostulationEntity[];
}
