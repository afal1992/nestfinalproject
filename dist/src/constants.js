"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.POSTULATION_STATE_ARRAY = exports.STATE_REFUSED_POSTULATION = exports.STATE_PENDING_POSTULATION = exports.STATE_ACCEPT_POSTULATION = exports.STATE_ARRAY = exports.STATE_EXPIRED = exports.STATE_ACTIVE = exports.PRIORITY_ARRAY = exports.PRIORITY_HIGH = exports.PRIORITY_LOW = exports.PRIORITY_NORMAL = exports.OAUTH_EMAIL = exports.OAUTH_GOOGLE = void 0;
exports.OAUTH_GOOGLE = 'OAUTH_GOOGLE';
exports.OAUTH_EMAIL = 'OAUTH_EMAIL';
exports.PRIORITY_NORMAL = { id: 'normal', label: 'priority_normal' };
exports.PRIORITY_LOW = { id: 'low', label: 'priority_low' };
exports.PRIORITY_HIGH = { id: 'high', label: 'priority_high' };
exports.PRIORITY_ARRAY = [exports.PRIORITY_HIGH, exports.PRIORITY_LOW, exports.PRIORITY_NORMAL];
exports.STATE_ACTIVE = { id: 'active', label: 'state_active' };
exports.STATE_EXPIRED = { id: 'expired', label: 'state_expired' };
exports.STATE_ARRAY = [exports.STATE_ACTIVE, exports.STATE_EXPIRED];
exports.STATE_ACCEPT_POSTULATION = 'accept';
exports.STATE_PENDING_POSTULATION = 'pending';
exports.STATE_REFUSED_POSTULATION = 'refused';
exports.POSTULATION_STATE_ARRAY = [
    exports.STATE_ACCEPT_POSTULATION,
    exports.STATE_PENDING_POSTULATION,
    exports.STATE_REFUSED_POSTULATION,
];
//# sourceMappingURL=constants.js.map