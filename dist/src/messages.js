"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GENERIC_SUCCESS = exports.GENERIC_ERROR = void 0;
const generic_response_1 = require("./oauth/model/generic-response");
exports.GENERIC_ERROR = new generic_response_1.GenericResponse(true, null, 'process_error');
exports.GENERIC_SUCCESS = new generic_response_1.GenericResponse(false, null, 'process_success');
//# sourceMappingURL=messages.js.map