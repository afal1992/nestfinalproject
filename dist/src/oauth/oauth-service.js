"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var OauthService_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.OauthService = void 0;
const common_1 = require("@nestjs/common");
const user_entity_1 = require("../database/model/user.entity");
const typeorm_1 = require("typeorm");
const generic_response_1 = require("./model/generic-response");
const class_validator_1 = require("class-validator");
const messages_1 = require("../messages");
const jwt_util_service_1 = require("./jwt-util-service");
const axios_1 = require("axios");
const config_1 = require("@nestjs/config");
const typeorm_2 = require("@nestjs/typeorm");
let OauthService = OauthService_1 = class OauthService {
    constructor(userRepository, jwtUtilService, configService) {
        this.userRepository = userRepository;
        this.jwtUtilService = jwtUtilService;
        this.configService = configService;
        this.logger = new common_1.Logger(OauthService_1.name);
        this.validateOauthGoogle = async (accessToken) => {
            try {
                const urlCheckGoogle = this.configService.get('URL_CHECK_GOOGLE') ||
                    'https://www.googleapis.com/oauth2/v3/userinfo?access_token=';
                if (!urlCheckGoogle)
                    return null;
                const response = await axios_1.default.get(`${urlCheckGoogle}${accessToken}`, {
                    timeout: 5000000,
                });
                if (!response || !response.data)
                    return null;
                return response.data;
            }
            catch (e) {
                this.logger.error(e);
                return null;
            }
        };
    }
    async singIn(request) {
        try {
            const { email } = request;
            const conditions = {
                where: {
                    email,
                },
            };
            const responseSigIn = await this.userRepository.find(conditions);
            if (!responseSigIn || !responseSigIn[0])
                return messages_1.GENERIC_ERROR;
            const user = responseSigIn[0];
            const payload = {
                id: user.id,
                name: user.name,
                email: user.email,
                environment: 'TEST',
                picture: '',
            };
            const accessToken = await this.jwtUtilService.generateAccessToken(payload);
            const response = { user: payload, accessToken };
            return new generic_response_1.GenericResponse(false, response, 'sucessfull_message');
        }
        catch (e) {
            return messages_1.GENERIC_ERROR;
        }
    }
    async saveUser(responseGoogleLogin, environment) {
        try {
            const errorArray = await (0, class_validator_1.validate)(responseGoogleLogin);
            if (errorArray && errorArray.length > 0) {
                this.logger.error('Err save user', errorArray);
                return null;
            }
            const user = new user_entity_1.UserEntity(responseGoogleLogin.name, responseGoogleLogin.email, environment, responseGoogleLogin.picture);
            return await this.userRepository.save(user);
        }
        catch (e) {
            return null;
        }
    }
    async singInGoogle(request) {
        try {
            const { accessTokenOauth } = request;
            const responseValidateOauth = await this.validateOauthGoogle(accessTokenOauth);
            if (!responseValidateOauth)
                return messages_1.GENERIC_ERROR;
            const conditions = {
                where: {
                    email: responseValidateOauth.email,
                },
            };
            const responseSigIn = await this.userRepository.find(conditions);
            let user = responseSigIn && responseSigIn[0] ? responseSigIn[0] : null;
            if (!user) {
                user = await this.saveUser(responseValidateOauth, request.environment);
            }
            else {
                if (request.environment !== user.environment)
                    return messages_1.GENERIC_ERROR;
            }
            if (!user)
                return messages_1.GENERIC_ERROR;
            const payload = {
                id: user.id,
                name: user.name,
                email: user.email,
                environment: user.environment,
                picture: user.picture,
            };
            const accessToken = await this.jwtUtilService.generateAccessToken(payload);
            user.password = '';
            const response = { user: payload, accessToken };
            return new generic_response_1.GenericResponse(false, response, 'sucessfull_message');
        }
        catch (e) {
            return messages_1.GENERIC_ERROR;
        }
    }
    async getPublicationPage(page) {
        try {
            return new generic_response_1.GenericResponse(false, [page], '');
        }
        catch (e) {
            return new generic_response_1.GenericResponse(false, [], '');
        }
    }
};
OauthService = OauthService_1 = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_2.InjectRepository)(user_entity_1.UserEntity)),
    __metadata("design:paramtypes", [typeorm_1.Repository,
        jwt_util_service_1.JwtUtilService,
        config_1.ConfigService])
], OauthService);
exports.OauthService = OauthService;
//# sourceMappingURL=oauth-service.js.map