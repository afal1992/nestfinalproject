import { UserEntity } from 'src/database/model/user.entity';
import { Repository } from 'typeorm';
import { GenericResponse } from './model/generic-response';
import { JwtUtilService } from './jwt-util-service';
import { SignInRequest } from './model/signin-request';
import { ConfigService } from '@nestjs/config';
export interface IUserSession {
    id: string;
    name: string;
    email: string;
    environment: string;
    picture: string;
}
export declare class OauthService {
    private userRepository;
    private readonly jwtUtilService;
    private configService;
    private readonly logger;
    constructor(userRepository: Repository<UserEntity>, jwtUtilService: JwtUtilService, configService: ConfigService);
    singIn(request: SignInRequest): Promise<GenericResponse | null>;
    private saveUser;
    singInGoogle(request: SignInRequest): Promise<GenericResponse | null>;
    getPublicationPage(page: number): Promise<GenericResponse>;
    private validateOauthGoogle;
}
