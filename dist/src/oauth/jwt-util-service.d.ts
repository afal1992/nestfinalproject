import { JwtService } from '@nestjs/jwt';
import { IUserSession } from './oauth-service';
export declare class JwtUtilService {
    private jwtService;
    constructor(jwtService: JwtService);
    generateAccessToken(user: IUserSession): Promise<string>;
}
