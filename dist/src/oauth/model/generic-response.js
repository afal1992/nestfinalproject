"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GenericResponse = void 0;
class GenericResponse {
    constructor(error, data, message) {
        this.error = false;
        this.error = error;
        this.data = data;
        this.message = message;
    }
}
exports.GenericResponse = GenericResponse;
//# sourceMappingURL=generic-response.js.map