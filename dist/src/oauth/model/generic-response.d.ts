export declare class GenericResponse {
    error: boolean;
    data: any;
    message: string;
    constructor(error: boolean, data: any, message: string);
}
