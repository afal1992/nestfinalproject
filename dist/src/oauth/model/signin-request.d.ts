export declare class SignInRequest {
    email: string;
    password: string;
    accessTokenOauth: string;
    environment: string;
}
