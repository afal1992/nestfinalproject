"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OauthController = void 0;
const common_1 = require("@nestjs/common");
const passport_1 = require("@nestjs/passport");
const customer_service_1 = require("../customer/customer-service");
const signin_request_1 = require("./model/signin-request");
const oauth_service_1 = require("./oauth-service");
let OauthController = class OauthController {
    constructor(oauthService, customerService) {
        this.oauthService = oauthService;
        this.customerService = customerService;
        console.log(this.customerService.getHealthy());
    }
    async signIn(body) {
        return await this.oauthService.singInGoogle(body);
    }
    async getPublicationPage(req, page) {
        const { user } = req;
        console.log('User2...', user);
        return await this.oauthService.getPublicationPage(page);
    }
};
__decorate([
    (0, common_1.Post)('/signIn'),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [signin_request_1.SignInRequest]),
    __metadata("design:returntype", Promise)
], OauthController.prototype, "signIn", null);
__decorate([
    (0, common_1.Get)('/getPublicationPage'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('jwt')),
    __param(0, (0, common_1.Request)()),
    __param(1, (0, common_1.Query)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], OauthController.prototype, "getPublicationPage", null);
OauthController = __decorate([
    (0, common_1.Controller)('oauth'),
    __metadata("design:paramtypes", [oauth_service_1.OauthService,
        customer_service_1.CustomerService])
], OauthController);
exports.OauthController = OauthController;
//# sourceMappingURL=oauth-controller.js.map