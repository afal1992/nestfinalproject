"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OauthModule = void 0;
const common_1 = require("@nestjs/common");
const database_module_1 = require("../database/database.module");
const jwt_util_service_1 = require("./jwt-util-service");
const oauth_controller_1 = require("./oauth-controller");
const oauth_service_1 = require("./oauth-service");
const jwt_1 = require("@nestjs/jwt");
const passport_1 = require("@nestjs/passport");
const jwt_strategy_1 = require("./jwt.strategy");
const customer_module_1 = require("../customer/customer.module");
const user_entity_1 = require("../database/model/user.entity");
const typeorm_1 = require("@nestjs/typeorm");
let OauthModule = class OauthModule {
};
OauthModule = __decorate([
    (0, common_1.Module)({
        controllers: [oauth_controller_1.OauthController],
        imports: [
            database_module_1.DatabaseModule,
            passport_1.PassportModule,
            customer_module_1.CustomerModule,
            typeorm_1.TypeOrmModule.forFeature([user_entity_1.UserEntity], 'default'),
            jwt_1.JwtModule.register({
                secret: process.env.JWT_SECRET || 'afal1992',
                signOptions: { expiresIn: '8h' },
            }),
        ],
        providers: [oauth_service_1.OauthService, jwt_util_service_1.JwtUtilService, jwt_strategy_1.JwtStrategy],
    })
], OauthModule);
exports.OauthModule = OauthModule;
//# sourceMappingURL=oauth.module.js.map