import { CustomerService } from 'src/customer/customer-service';
import { GenericResponse } from './model/generic-response';
import { SignInRequest } from './model/signin-request';
import { OauthService } from './oauth-service';
export interface ISignIn {
    email: string;
    password: string;
    oauthType: string;
    oauthAccessToken: string;
}
export interface ISignUp {
    email: string;
    password: string;
    name: string;
    type?: string;
}
export interface ISignInGoogle {
    email: string;
    password: string;
    oauthType: string;
    oauthAccessToken: string;
}
export declare class OauthController {
    private readonly oauthService;
    private customerService;
    constructor(oauthService: OauthService, customerService: CustomerService);
    signIn(body: SignInRequest): Promise<GenericResponse | null>;
    getPublicationPage(req: any, page: any): Promise<GenericResponse | null>;
}
