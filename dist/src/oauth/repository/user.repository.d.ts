import { UserEntity } from 'src/database/model/user.entity';
import { Connection } from 'typeorm';
export declare const userRepository: {
    provide: string;
    useFactory: (connection: Connection) => import("typeorm").Repository<UserEntity>;
    inject: string[];
}[];
