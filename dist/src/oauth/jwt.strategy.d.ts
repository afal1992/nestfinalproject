import { IUserSession } from './oauth-service';
declare const JwtStrategy_base: new (...args: any[]) => any;
export declare class JwtStrategy extends JwtStrategy_base {
    constructor();
    validate(payload: IUserSession): Promise<IUserSession>;
}
export {};
