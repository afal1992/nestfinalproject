export declare const OAUTH_GOOGLE = "OAUTH_GOOGLE";
export declare const OAUTH_EMAIL = "OAUTH_EMAIL";
export declare const PRIORITY_NORMAL: {
    id: string;
    label: string;
};
export declare const PRIORITY_LOW: {
    id: string;
    label: string;
};
export declare const PRIORITY_HIGH: {
    id: string;
    label: string;
};
export declare const PRIORITY_ARRAY: {
    id: string;
    label: string;
}[];
export declare const STATE_ACTIVE: {
    id: string;
    label: string;
};
export declare const STATE_EXPIRED: {
    id: string;
    label: string;
};
export declare const STATE_ARRAY: {
    id: string;
    label: string;
}[];
export declare const STATE_ACCEPT_POSTULATION = "accept";
export declare const STATE_PENDING_POSTULATION = "pending";
export declare const STATE_REFUSED_POSTULATION = "refused";
export declare const POSTULATION_STATE_ARRAY: string[];
